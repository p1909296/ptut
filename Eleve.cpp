#include "Eleve.h"

Eleve::Eleve():numero_etu(""),nom(""),prenom(""),groupe(""),sous_groupe("")
{
}
Eleve::Eleve(std::string num_etu, std::string last_name, std::string name, std::string group, std::string sub_group) : numero_etu(num_etu), nom(last_name), prenom(name), groupe(group), sous_groupe(sub_group)
{
}
Eleve::~Eleve()
{
}

std::string Eleve::get_nom()
{
	return nom;
}

std::string Eleve::get_prenom()
{
	return prenom;
}

std::string Eleve::get_groupe()
{
	return groupe;
}

std::string Eleve::get_sous_groupe()
{
	return sous_groupe;
}
